import os
import cv2
import pytesseract
import imageproc as ip
from difflib import SequenceMatcher

def preprocess_image(filename, threshold_lower=127, scale_multiplier=5, scale_interpolation=cv2.INTER_CUBIC, debug=False):
    """Preprocess image for OCR."""
    if debug:
        filename_noext = os.path.splitext(os.path.split(filename)[1])[0]
        if os.path.isdir('ocr_debug_out') == False:
            os.mkdir('ocr_debug_out')

    image = cv2.imread(filename, 1)
    image = ip.invert(image)
    image = ip.grayscale(image)
    if debug:
        cv2.imwrite(f'ocr_debug_out/{filename_noext}_1_invert.png', image)

    image = ip.scale(image, multiplier=scale_multiplier, interpolation=scale_interpolation)
    if debug:
        cv2.imwrite(f'ocr_debug_out/{filename_noext}_2_scale.png', image)

    image = ip.threshold(image, lower=threshold_lower)
    if debug:
        cv2.imwrite(f'ocr_debug_out/{filename_noext}_3_threshold.png', image)

    image = ip.blobfill(image, multiplier=scale_multiplier)
    if debug:
        cv2.imwrite(f'ocr_debug_out/{filename_noext}_4_blobfill.png', image)

    image = ip.opening(image)
    if debug:
        cv2.imwrite(f'ocr_debug_out/{filename_noext}_5_opening.png', image)

    return image


def ocr_image(filename, threshold_lower=127, scale_multiplier=2.9, scale_interpolation=cv2.INTER_CUBIC, debug=False):
    """Perform OCR on an image file."""
    image = preprocess_image(filename, threshold_lower=threshold_lower, scale_multiplier=scale_multiplier, scale_interpolation=scale_interpolation, debug=debug)

    tesseract_config = r"""
    --oem 3
    --psm 4
    -c tessedit_char_whitelist=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-0123456789
    """

    ocr = pytesseract.image_to_string(image, lang='eng', config=tesseract_config)

    terms = [term.strip() for term in ocr.split("\n")]
    return [term for term in terms if term != ''], image


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def ocr_auto_learn(filename: str, database: dict, ratio_cutoff: float = 0.8) -> (set, dict, set):
    """Perform OCR on image and match against a database, updating when a new fuzzy match is found.

    Database format:
    {(key1_exact: str, [(key1_fuzzy1: str, ratio: float), (key1_fuzzy2: str, ratio: int), ...]), ...}

    Returns:
    - all matched terms
    - fuzzy matched terms in the format: {(term_exact: str, (term_fuzzy: str, ratio: int, is_new: bool)), ...}
    - terms not matched
    """
    ocr_result, _ = ocr_image(filename)

    test = set(ocr_result)
    exact = set(list(database))
    matches = test & exact
    unknowns = test - matches

    # perfect match
    if len(unknowns) == 0:
        return matches, {}, set()

    # reverse database to the form:
    # {(fuzzy_key: str, (exact_key: str, ratio: float)), ...}
    reverse_database = {}
    for key, values in database.items():
        for fuzzy_key, ratio in values:
            reverse_database[fuzzy_key] = (key, ratio)

    fuzzy_matches = {}
    remaining = exact - matches

    for term in unknowns.copy():
        # previously fuzzy matched
        if term in reverse_database:
            found_key, found_ratio = reverse_database[term]
            fuzzy_matches[found_key] = (term, found_ratio, False)
            matches.add(found_key)
            unknowns.remove(term)
            continue

        # attempt fuzzy match
        similars = dict([(x, similar(x, term)) for x in remaining])
        found_key, found_ratio = max(similars.items(), key=lambda x: x[1])
        if found_ratio > ratio_cutoff:
            fuzzy_matches[found_key] = (term, found_ratio, True)
            matches.add(found_key)
            database[found_key].append((term, found_ratio))
            unknowns.remove(term)

    return matches, fuzzy_matches, unknowns
