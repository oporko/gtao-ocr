import ocr

sample_lines = open("samples/samples.txt").readlines()
samples = [(f"{i + 1}.png", sample_lines[i]) for i in range(len(sample_lines))]

database = {}

for i in range(len(samples)):

    # update database
    for term in sample_lines[i].split(" "):
        term = term.strip()
        if term not in database:
            database[term] = []

    matches, fuzzy, unknown = ocr.ocr_auto_learn(f"samples/{i + 1}.png", database)

    print(f"Sample {i}, matches: {len(matches)}, fuzzy: {fuzzy}, unknowns: {unknown}")
