import cv2
import pytesseract
import ocr
from difflib import SequenceMatcher

# config
debug = True
threshold_lower = 127
scale_multiplier = 2.9
scale_interpolation = cv2.INTER_CUBIC
max_samples = -1

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

# start

sample_lines = open("samples/samples.txt").readlines()
samples = [(f"{i + 1}.png", sample_lines[i]) for i in range(len(sample_lines))]

total_accuracy = 0
total_lines = 0

for i in range(len(samples)):
    if max_samples > 0 and i >= max_samples:
        break

    filename, expected_line = samples[i]
    actual, image = ocr.ocr_image(f"samples/{filename}", threshold_lower=threshold_lower, scale_multiplier=scale_multiplier, scale_interpolation=scale_interpolation, debug=debug)
    expected = [line.strip() for line in expected_line.split(" ")]

    actual = [term.lower() for term in actual]
    expected = [term.lower() for term in expected]

    # TODO separate detailed logging

    print(f"Sample {i + 1}")

    accuracy = 0
    lines = 0

    for j in range(len(expected)):
        if len(actual) <= j:
            print(f"BAD (empty) != {expected[j]}")
            pass
        elif expected[j] == actual[j]:
            accuracy += 1
            # print(f"OK {actual[j]}")
        else:
            print(f"BAD {actual[j]} != {expected[j]} {similar(actual[j], expected[j])}")
            pass

    print(f"Sample {i + 1} accuracy: {round(accuracy / len(expected) * 100, 1)}%")

    total_accuracy += accuracy
    total_lines += len(expected)

    # print()


print(f"Overall accuracy: {round(total_accuracy / total_lines * 100, 1)}%")
